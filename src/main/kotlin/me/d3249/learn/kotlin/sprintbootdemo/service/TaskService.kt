package me.d3249.learn.kotlin.sprintbootdemo.service

import me.d3249.learn.kotlin.sprintbootdemo.entity.Task
import me.d3249.learn.kotlin.sprintbootdemo.repository.TaskRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class TaskService(val repository: TaskRepository) {

    fun getAll() = repository.findAll()

    fun saveOne(task: Task) = repository.save(task)

    fun deleteOne(id: Long): Boolean {
        val found = repository.existsById(id)

        if (found) {
            repository.deleteById(id)
        }

        return found
    }
}