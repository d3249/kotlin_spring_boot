package me.d3249.learn.kotlin.sprintbootdemo.controller

import me.d3249.learn.kotlin.sprintbootdemo.entity.Task
import me.d3249.learn.kotlin.sprintbootdemo.service.TaskService
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/task")
class TaskController(val service: TaskService) {

    @GetMapping
    fun getTasks(): Iterable<Task> = service.getAll()

    @PostMapping
    fun saveOne(@RequestBody task: Task) = service.saveOne(task)

    @DeleteMapping("/{id}")
    fun deleteOne(@PathVariable id: Long): ResponseEntity<String> =
            if (service.deleteOne(id)) {
                ResponseEntity.ok("Task $id deleted")
            } else {
                ResponseEntity.status(NOT_FOUND).body("Task $id not found")
            }

}
