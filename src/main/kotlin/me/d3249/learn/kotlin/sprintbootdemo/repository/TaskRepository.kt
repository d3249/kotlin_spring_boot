package me.d3249.learn.kotlin.sprintbootdemo.repository

import me.d3249.learn.kotlin.sprintbootdemo.entity.Task
import org.springframework.data.repository.CrudRepository

interface TaskRepository : CrudRepository<Task, Long>