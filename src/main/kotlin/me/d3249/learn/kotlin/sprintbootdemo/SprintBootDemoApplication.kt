package me.d3249.learn.kotlin.sprintbootdemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SprintBootDemoApplication

fun main(args: Array<String>) {
	runApplication<SprintBootDemoApplication>(*args)
}
